
from gaelib.ngforms import *


class NewRequestForm(Form):
  @property
  def fields(self):
    return [
      InputField(id='whereinput', name='whereinput', placeholder='',
          cls=['input-xlarge']),
      InputField(id='whatinput', name='whatinput', placeholder='',
          cls=['input-xlarge']),
      InputField(id='whytextarea', name='whytextarea', placeholder='',
          cls=['input-xlarge']),
      SubmitField(label='Create'),
    ]

  @property
  def validations(self):
    return {
      'whereinput': [
        Required('where is required'),
      ],
      'whatinput': [
        Required('what is required'),
      ],
      'whytextarea': [
        Required('why is required'),
      ],
    }
