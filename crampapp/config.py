
import os


DEBUG = True

# Set DEBUG to false if we are not using the development server
if 'SERVER_SOFTWARE' in os.environ:
  from google.appengine.api import app_identity
  DEBUG = os.environ['SERVER_SOFTWARE'].startswith('Dev')

# The template path
TEMPLATE_DIR = 'client/templates/'

# Depends of if we are on dev mode we use the localhost url or the real one
if DEBUG:
  HOST = 'http://localhost:8080'
else:
  HOST = 'https://%s.appspot.com' % app_identity.get_application_id()

APP_CONFIG = {}
APP_CONFIG['webapp2_extras.sessions'] = {
    'secret_key': 'a74f912ef930ea74f912ef930ec2eca6cd4b0f78a9120c2eca6cd4b9120',
}
