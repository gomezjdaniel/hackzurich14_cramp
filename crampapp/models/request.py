
from google.appengine.ext import ndb


class Request(ndb.Model):
  where = ndb.StringProperty()
  what = ndb.StringProperty(indexed=False)
  why = ndb.StringProperty(indexed=False)
  User = ndb.StringProperty()
