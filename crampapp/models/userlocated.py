
from google.appengine.ext import ndb


class UserLocated(ndb.Model):
  where = ndb.StringProperty(indexed=False)
  User = ndb.StringProperty()

  @classmethod
  def return_all_useremails_from_location(cls, where):
    models = UserLocated.query(UserLocated.where == where)
    objs = []
    for model in models:
      objs.append(model.User)

    if not objs:
      return None

    return objs
  
