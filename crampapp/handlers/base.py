
from gaelib import handlers

from google.appengine.api import users

from crampapp.models.userlocated import UserLocated

import webapp2


# BaseHandler which loads the main page through GET request to /
class BaseHandler(handlers.Base):
  def get(self):
    if users.get_current_user() is None:
      self.render('index_invited.html', login_url=users.create_login_url('/'))
    else:
      user_datastore = UserLocated.query(UserLocated.User == users.get_current_user().email())
      if not user_datastore:
        new_user = UserLocated(User=user.get_current_user().email(), where='Zurich, Switzerland')
        new_user.put()

      self.render('index_signed.html', logout_url=users.create_logout_url('/'))
