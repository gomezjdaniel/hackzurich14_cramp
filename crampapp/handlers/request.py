
from gaelib import handlers

from crampapp.models.request import Request
from crampapp.models.userlocated import UserLocated

from crampapp.forms.request import *

from google.appengine.api import users,mail

import webapp2


class NewRequestHandler(handlers.Base):
  def post(self):
    data = NewRequestForm().validate()

    new_request = Request(User=users.get_current_user().email(), where=data['whereinput'],
        what=data['whatinput'], why=data['whytextarea'])
    r_key = new_request.put();

    users_email = UserLocated.return_all_useremails_from_location(data['whereinput'])
    #users_email = ['mahakmaheshwari19@gmail.com']
    for email in users_email:
      message = mail.EmailMessage()
      message.sender = 'Cramp app <noreply@crampapp.appspotmail.com>'
      message.to = email
      message.subject = 'Can you help Peter?. He needs (a) %s' % data['whatinput']
      message.body = """
%s

Help Peter clicking on the next link https://crampapp.appspot.com/request/accept/%s

-----------

Cheers,
the CrampApp team
""" % (data['whytextarea'], r_key.id())
  
      message.send()

    self.json({'error': ''})


class AcceptRequestHandler(handlers.Base):
  def get(self, rid):
    rid = int(rid)

    request = Request.get_by_id(rid)

    message = mail.EmailMessage()
    message.sender = 'Cramp app <noreply@crampapp.appspotmail.com>'
    message.to = request.User
    message.cc = users.get_current_user().email()
    message.subject = '%s wants to help you! :D' % users.get_current_user().nickname()
    message.body = '''
Hi Peter!,

We introduce you (email is on cc) %s.. Get to it!


Cheers,
the CrampApp team
''' % users.get_current_user().nickname()

    message.send()

    self.render('accept_request.html', logout_url=users.create_logout_url('/'))
