
import webapp2
import os

import config


# Routes
router = [
  (r'/', 'base.BaseHandler'),
  (r'/request/new', 'request.NewRequestHandler'),
  (r'/request/accept/(\d+)', 'request.AcceptRequestHandler'),
]

# Do not touch from here
routes = (
    [webapp2.SimpleRoute(p[0], handler='crampapp.handlers.%s' % p[1]) 
    for p in router])

app = webapp2.WSGIApplication(routes, debug=config.DEBUG,
    config=config.APP_CONFIG)


def main():
  app.run()


if __name__ == '__main__':
  main()
