$(function () {
  $('#whereinput').focus();

  $('#whereinput').keypress(function (e) {
    var key = e.which;
    if(key == 13 && $('#whereinput').val().length != 0) {
      $('#whatinput').fadeIn( "slow" );
      $('#whatinput').focus();
    }
  });

  $('#whatinput').keypress(function (e) {
    var key = e.which;
    if(key == 13 && $('#whatinput').val().length != 0) {
      $('#whytextarea').fadeIn( "slow" );
      $('#sendrequest').fadeIn( "slow" );
      $('#sendtruerequest').fadeIn( "slow" );
      $('#whytextarea').focus();
    }
  });

  $('#sendtruerequest').click(function (e) {
    var fdata = {
      whereinput: $('#whereinput').val(),
      whatinput: $('#whatinput').val(),
      whytextarea: $('#whytextarea').val(),
    };

    $.ajax({
        url: '/request/new',
        method: 'POST',
        dataType : 'application/json; charset=utf-8',
        data : JSON.stringify(fdata),
        dataFilter: function(data) {
          data = data.replace(')]}\',', '');
          return data;
        }
      });

      $('#requestFormContainer').fadeOut('slow', function() {
        $('#sentRequestConfirmation').fadeIn('slow');
      });
  });

  // textarea maxlength
  var maxLen = 167;
  
  $('#whytextarea').keypress(function(event){
      var Length = $("#whytextarea").val().length;
      var AmountLeft = maxLen - Length;
      if(Length >= maxLen){
          if (event.which != 8) {
              return false;
          }
      }
  });

  $('#whytextarea').click(function() {
      type('#whytextarea', "I'm in a hackathon in technopark close to Hardbruke and don't want to sleep on the cold hard floor! I've come allllll the way from Manchester(I'm actually a kiwi) and haven't slept in 24 hours. Cheers, Pete");
    });

    function type(element, text) {
      if (type.arguments[2]) {
        current = type.arguments[2];
      } else {
        current = 0;
      }
      if (text.length > current) {
        current = current + 1;
        $(element).val(text.substr(0, current));
        setTimeout(function() {
          type(element, text, current);
        }, 25);
      }
    }
});
